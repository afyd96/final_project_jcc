import 'package:final_project_pay/Screen/non_logged/Station/widget/LoginAndRegister.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Station extends StatelessWidget {
  const Station({Key? key}) : super(key: key);
  static String routeName = "Screen/non_logged/Station";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SecondaryColor,
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: <Widget>[
            Image.asset(
              "assets/images/onlineStore.png",
              height: MediaQuery.of(context).size.height * 0.5,
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "Pay for the Best\n Price and Quality",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: PrimaryDarkColor,
                fontSize: 28,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            LoginAndRegister(),
            SizedBox(
              height: 20,
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: SecondaryColor,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                          side: BorderSide(color: PrimaryColor, width: 2))),
                  onPressed: () {},
                  child: Container(
                    padding: EdgeInsets.all(8),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        SvgPicture.asset(
                          "assets/icons/google.svg",
                          height: 30,
                          width: 30,
                        ),
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          "Connect with Google",
                          style: TextStyle(fontSize: 20, color: TextLightColor),
                        )
                      ],
                    ),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
