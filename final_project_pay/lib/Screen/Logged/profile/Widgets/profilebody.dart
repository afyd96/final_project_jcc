import 'package:final_project_pay/Screen/Logged/profile/Widgets/profilemenu.dart';
import 'package:final_project_pay/Screen/Logged/profile/Widgets/profilepict.dart';
import 'package:final_project_pay/Screen/non_logged/Login/LoginScreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class profileBody extends StatelessWidget {
  const profileBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    User? user = FirebaseAuth.instance.currentUser;
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(vertical: 20),
      child: Column(
        children: [
          ProfilePict(),
          SizedBox(height: 20),
          profileMenu(
            text: "My Account",
            icon: "assets/icons/profile-user.svg",
            press: () => {},
          ),
          profileMenu(
            text: "Notifications",
            icon: "assets/icons/bell.svg",
            press: () {},
          ),
          profileMenu(
            text: "Settings",
            icon: "assets/icons/settings.svg",
            press: () {},
          ),
          profileMenu(
            text: "Log Out",
            icon: "assets/icons/logout.svg",
            press: () async {
              await FirebaseAuth.instance.signOut();
              Navigator.pushNamedAndRemoveUntil(
                  context, LoginScreen.routeName, (route) => false);
            },
          ),
        ],
      ),
    );
  }
}
