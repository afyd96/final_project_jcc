import 'package:final_project_pay/Screen/non_logged/Splash/Splash_Screen.dart';
import 'package:final_project_pay/shared/constant/routes.dart';
import 'package:final_project_pay/shared/constant/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Pay',
      debugShowCheckedModeBanner: false,
      theme: themeData(),
      // home: const SplashScreen(),
      // Saya menggunakan routename
      initialRoute: SplashScreen.routeName,
      routes: routes,
    );
  }
}
