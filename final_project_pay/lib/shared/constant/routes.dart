import 'package:final_project_pay/Screen/Logged/Cart/CartScreen.dart';
import 'package:final_project_pay/Screen/Logged/Detail/DetailScreen.dart';
import 'package:final_project_pay/Screen/Logged/Home/Home.dart';
import 'package:final_project_pay/Screen/Logged/profile/profile.dart';
import 'package:final_project_pay/Screen/non_logged/ForgotPassword/ForgotPassword.dart';
import 'package:final_project_pay/Screen/non_logged/Login/LoginScreen.dart';
import 'package:final_project_pay/Screen/non_logged/Register/Register.dart';
import 'package:final_project_pay/Screen/non_logged/Splash/Splash_Screen.dart';
import 'package:final_project_pay/Screen/non_logged/Station/station.dart';
import 'package:flutter/widgets.dart';
import 'package:final_project_pay/Screen/Logged/product/product.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  Station.routeName: (context) => Station(),
  LoginScreen.routeName: (context) => LoginScreen(),
  ForgotPassword.routeName: (context) => ForgotPassword(),
  Register.routeName: (context) => Register(),
  Home.routeName: (context) => Home(),
  CartScreen.routeName: (context) => CartScreen(),
  DetailScreen.routeName: (context) => DetailScreen(),
  Product.routeName: (context) => Product(),
  Profile.nameRoute: (context) => Profile(),
};
