import 'package:final_project_pay/Screen/Logged/Detail/DetailScreen.dart';
import 'package:final_project_pay/models/product_model.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ProductCard extends StatelessWidget {
  const ProductCard({
    Key? key,
    this.width = 140,
    this.aspectRetio = 0.5,
    required this.product,
  }) : super(key: key);

  final double width, aspectRetio;
  final Product product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20),
      child: SizedBox(
        width: MediaQuery.of(context).size.width * 0.4,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            AspectRatio(
              aspectRatio: 1.02,
              child: Container(
                padding: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: SecondaryColor.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Hero(
                  tag: product.id.toString(),
                  child: Image.asset(product.images[0]),
                ),
              ),
            ),
            const SizedBox(height: 10),
            Text(
              product.title,
              style: TextStyle(color: TextLightColor),
              maxLines: 2,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "\$${product.price}",
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: PrimaryColor,
                  ),
                ),
                TextButton(
                    onPressed: () {
                      Navigator.pushNamed(
                        context,
                        DetailScreen.routeName,
                        arguments: ProductDetailsArguments(product: product),
                      );
                    },
                    child: Text(
                      "Buy",
                      style: TextStyle(fontSize: 20, color: PrimaryLightColor),
                    )),
                InkWell(
                  borderRadius: BorderRadius.circular(50),
                  onTap: () {},
                  child: Container(
                    padding: EdgeInsets.all(8),
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                      color: product.isFavourite
                          ? PrimaryDarkColor.withOpacity(0.15)
                          : PrimaryLightColor.withOpacity(0.1),
                      shape: BoxShape.circle,
                    ),
                    child: SvgPicture.asset(
                      "assets/icons/heart.svg",
                      color: product.isFavourite
                          ? PrimaryLightColor
                          : PrimaryDarkColor,
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
