import 'package:final_project_pay/Screen/Logged/Detail/Widgets/ProductDesc.dart';
import 'package:final_project_pay/Screen/Logged/Detail/Widgets/ProductImages.dart';
import 'package:final_project_pay/Screen/Logged/Detail/Widgets/TopRound.dart';
import 'package:final_project_pay/Screen/Logged/Detail/Widgets/colors.dart';
import 'package:final_project_pay/models/product_model.dart';
import 'package:final_project_pay/shared/components/defaultButton.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  final Product product;

  const Body({Key? key, required this.product}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ProductImages(product: product),
        TopRoundedContainer(
          color: Colors.white,
          child: Column(
            children: [
              ProductDescription(
                product: product,
                pressOnSeeMore: () {},
              ),
              TopRoundedContainer(
                color: Color(0xFFF6F7F9),
                child: Column(
                  children: [
                    ColorDots(product: product),
                    TopRoundedContainer(
                      color: Colors.white,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(
                          15,
                          15,
                          40,
                          15,
                        ),
                        child: DefaultButton(
                          text: "Add To Cart",
                          press: () {},
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
