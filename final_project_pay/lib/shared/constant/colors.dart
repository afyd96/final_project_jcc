import 'package:flutter/material.dart';

Color PrimaryColor = Color(0xFFB71C1C);

Color PrimaryLightColor = Color(0xFFF05545);

Color TextLightColor = Color(0xFF000000);

Color PrimaryDarkColor = Color(0xFF7F0000);

Color TextDarkColor = Color(0xFFFFFFFF);

Color SecondaryColor = Color(0xFFFFF8E1);
