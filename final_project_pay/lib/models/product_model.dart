import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';

class Product {
  final int id;
  final String title, description;
  final List<String> images;
  final List<Color> colors;
  final double rating, price;
  final bool isFavourite, isPopular;

  Product({
    required this.id,
    required this.images,
    required this.colors,
    this.rating = 0.0,
    this.isFavourite = false,
    this.isPopular = false,
    required this.title,
    required this.price,
    required this.description,
  });
}

// demo Products

List<Product> demoProducts = [
  Product(
    id: 1,
    images: [
      "assets/images/sepatu.png",
      "assets/images/jas.png",
      "assets/images/gaun.png",
      "assets/images/laptop.png",
    ],
    colors: [
      PrimaryDarkColor,
      PrimaryLightColor,
      PrimaryDarkColor,
      PrimaryLightColor,
    ],
    title: "Sepatu Puma",
    price: 500,
    description: description,
    rating: 4.8,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 2,
    images: [
      "assets/images/jas.png",
    ],
    colors: [
      PrimaryDarkColor,
      PrimaryLightColor,
      PrimaryDarkColor,
      PrimaryLightColor,
    ],
    title: "Jas Hitam",
    price: 50.5,
    description: description,
    rating: 4.1,
    isPopular: true,
  ),
  Product(
    id: 3,
    images: [
      "assets/images/gaun.png",
    ],
    colors: [
      PrimaryDarkColor,
      PrimaryLightColor,
      PrimaryDarkColor,
      PrimaryLightColor,
    ],
    title: "Gaun Pengantin",
    price: 36.55,
    description: description,
    rating: 4.1,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 4,
    images: [
      "assets/images/laptop.png",
    ],
    colors: [
      PrimaryDarkColor,
      PrimaryLightColor,
      PrimaryDarkColor,
      PrimaryLightColor,
    ],
    title: "Laptop Eklesia",
    price: 20.20,
    description: description,
    rating: 4.1,
    isFavourite: true,
  ),
];

const String description =
    "Sepatu Puma keren bagus kuat dan cocok untuk berlari dalam track seperti ...";
