import 'package:final_project_pay/Screen/non_logged/Login/Widget/LoginForm.dart';
import 'package:final_project_pay/Screen/non_logged/Register/Register.dart';
import 'package:final_project_pay/shared/components/defaultButton.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:final_project_pay/shared/constant/theme.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);
  static String routeName = "lib/Screen/non_logged/Login";
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SecondaryColor,
      appBar: AppBar(
        title: Text(
          "Login",
          style: TextStyle(color: PrimaryDarkColor),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              //bagian atas
              Image.asset(
                "assets/images/login.png",
                height: MediaQuery.of(context).size.height * 0.2,
              ),
              Container(
                  margin: EdgeInsets.all(10),
                  alignment: Alignment.center,
                  child: Text(
                    "Welcome Back!",
                    style: TextStyle(
                        color: PrimaryColor,
                        fontSize: 24,
                        fontWeight: FontWeight.w700),
                  )),
              Container(
                  alignment: Alignment.center,
                  child: Text(
                    "Log in with your email and password",
                    textAlign: TextAlign.center,
                    style: TextStyle(color: PrimaryDarkColor, fontSize: 14),
                  )),
              SizedBox(
                height: 10,
              ),
              //bagian form
              LoginForm(),
              // Belum Punya Akun
              Row(
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(left: 50),
                      child: Text("Doesn't have an account?")),
                  SizedBox(width: 20),
                  TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, Register.routeName);
                      },
                      child: Text(
                        "Register",
                        style: TextStyle(color: PrimaryColor),
                      ))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
