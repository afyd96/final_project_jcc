import 'package:final_project_pay/Screen/Logged/Home/Widgets/body.dart';
import 'package:final_project_pay/shared/components/CustomNavBot.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  static String routeName = "Screen/Logged/Home";
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SecondaryColor,
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
      body: BodyHome(),
    );
  }
}
