import 'package:final_project_pay/Screen/Logged/Cart/CartScreen.dart';
import 'package:final_project_pay/Screen/Logged/Home/Widgets/Banner.dart';
import 'package:final_project_pay/Screen/Logged/Home/Widgets/Category.dart';
import 'package:final_project_pay/Screen/Logged/Home/Widgets/offers.dart';
import 'package:final_project_pay/Screen/Logged/Home/Widgets/product.dart';
import 'package:final_project_pay/shared/components/customiconcounter.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BodyHome extends StatelessWidget {
  const BodyHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SecondaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(height: 20),
              // Bagian Header
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //Bagian SearchField
                    Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      decoration: BoxDecoration(
                        color: SecondaryColor.withOpacity(0.1),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: TextField(
                        onChanged: (value) => print(value),
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 15),
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            hintText: "Search product",
                            prefixIcon: Icon(Icons.search)),
                      ),
                    ),
                    CustomIconCounter(
                        svgSrc: "assets/icons/shopping-cart.svg",
                        numOfitem: 0,
                        press: () =>
                            Navigator.pushNamed(context, CartScreen.routeName)),
                    CustomIconCounter(
                        svgSrc: "assets/icons/bell.svg",
                        numOfitem: 2,
                        press: () {}),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Text(
                "Happy Pay!",
                style: TextStyle(
                    color: PrimaryColor,
                    fontSize: 32,
                    fontWeight: FontWeight.w700),
              ),
              banner(),
              SizedBox(height: 10),
              Category(),
              SizedBox(height: 20),
              offers(),
              SizedBox(height: 20),
              product(),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }
}
