import 'package:final_project_pay/Screen/non_logged/ForgotPassword/Widget/ForgotPasswordForm.dart';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              SvgPicture.asset(
                "assets/icons/forgot.svg",
                height: MediaQuery.of(context).size.height * 0.2,
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.04),
              Text(
                "Forgot Password",
                style: TextStyle(
                  fontSize: 28,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "Please enter your email and we will send \nyou a link to return to your account",
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 30),
              ForgotPasswordForm(),
            ],
          ),
        ),
      ),
    );
  }
}
