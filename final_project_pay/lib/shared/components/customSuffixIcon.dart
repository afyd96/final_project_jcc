import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomSuffixIcon extends StatelessWidget {
  const CustomSuffixIcon({Key? key, required this.svgicon}) : super(key: key);

  final String svgicon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: SvgPicture.asset(
        svgicon,
        height: 10,
        width: 10,
      ),
    );
  }
}
