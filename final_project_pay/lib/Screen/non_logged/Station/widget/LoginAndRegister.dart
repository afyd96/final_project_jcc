import 'package:final_project_pay/Screen/non_logged/Login/LoginScreen.dart';
import 'package:final_project_pay/Screen/non_logged/Register/Register.dart';
import 'package:final_project_pay/shared/components/defaultButton.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';

class LoginAndRegister extends StatelessWidget {
  const LoginAndRegister({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Row(
        children: <Widget>[
          Expanded(
            child: DefaultButton(
              text: "Register",
              press: () {
                Navigator.pushNamed(context, Register.routeName);
              },
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
              child: DefaultButton(
                  text: "Login",
                  press: () {
                    Navigator.pushNamed(context, LoginScreen.routeName);
                  })),
        ],
      ),
    );
  }
}
