import 'package:final_project_pay/Screen/Logged/Home/Home.dart';
import 'package:final_project_pay/Screen/non_logged/ForgotPassword/ForgotPassword.dart';
import 'package:final_project_pay/shared/components/customSuffixIcon.dart';
import 'package:final_project_pay/shared/components/defaultButton.dart';
import 'package:final_project_pay/shared/components/formerror.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:final_project_pay/shared/constant/validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  //controller
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  // firebase
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  final _formKey = GlobalKey<FormState>();

  bool? remember = false;
  final List<String?> errors = [];

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildEmailFormField(),
          SizedBox(height: 10),
          buildPasswordFormField(),
          SizedBox(height: 10),
          FormError(errors: errors),
          // remember me dan forgot password
          Row(
            children: [
              Checkbox(
                value: remember,
                activeColor: PrimaryColor,
                onChanged: (value) {
                  setState(() {
                    remember = value;
                  });
                },
              ),
              Text("Remember me"),
              Spacer(),
              GestureDetector(
                onTap: () =>
                    Navigator.pushNamed(context, ForgotPassword.routeName),
                child: Text(
                  "Forgot Password",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          SizedBox(height: 20),
          DefaultButton(
            text: "Login",
            press: () async {
              if (_formKey.currentState!.validate()) {
                _formKey.currentState!.save();
                await _firebaseAuth
                    .signInWithEmailAndPassword(
                        email: _emailController.text,
                        password: _passwordController.text)
                    .then((uid) => {
                          Fluttertoast.showToast(msg: "Login Successful"),
                          Navigator.pushNamed(context, Home.routeName)
                        })
                    .catchError((e) {
                  Fluttertoast.showToast(msg: e!.message);
                });
              }
              ;
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      controller: _passwordController,
      obscureText: true,
      onSaved: (value) => _passwordController.text = value!,
      onChanged: (value) {
        setState(() {
          if (value.isNotEmpty) {
            removeError(error: kPassNullError);
          } else if (value.length >= 8) {
            removeError(error: kShortPassError);
          }
          return null;
        });
      },
      validator: (value) {
        setState(() {
          if (value!.isEmpty) {
            addError(error: kPassNullError);
          } else if (value.length < 8) {
            addError(error: kShortPassError);
          }
          return null;
        });
      },
      decoration: InputDecoration(
        labelText: "Password",
        hintText: "Enter your password",
        suffixIcon: CustomSuffixIcon(svgicon: "assets/icons/padlock.svg"),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      onSaved: (value) => _emailController.text = value!,
      onChanged: (value) {
        setState(() {
          if (value.isNotEmpty) {
            removeError(error: kEmailNullError);
          } else if (emailValidatorRegExp.hasMatch(value)) {
            removeError(error: kInvalidEmailError);
          }
          return null;
        });
      },
      validator: (value) {
        setState(() {
          if (value!.isEmpty) {
            addError(error: kEmailNullError);
          } else if (!emailValidatorRegExp.hasMatch(value)) {
            addError(error: kInvalidEmailError);
          }
          return null;
        });
      },
      decoration: InputDecoration(
          labelText: "Email",
          hintText: "Enter your email",
          suffixIcon: CustomSuffixIcon(svgicon: "assets/icons/email.svg")),
    );
  }
}
