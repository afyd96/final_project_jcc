import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';

ThemeData themeData() {
  return ThemeData(
    appBarTheme: appBarTema(),
    textTheme: TextTheme(headline6: TextStyle(color: PrimaryColor)),
    visualDensity: VisualDensity.adaptivePlatformDensity,
    inputDecorationTheme: inputDecoration(),
  );
}

InputDecorationTheme inputDecoration() {
  var outlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(20),
    borderSide: BorderSide(color: PrimaryColor),
    gapPadding: 10,
  );
  return InputDecorationTheme(
      labelStyle: TextStyle(color: PrimaryDarkColor),
      contentPadding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      enabledBorder: outlineInputBorder,
      focusedBorder: outlineInputBorder,
      border: outlineInputBorder);
}

AppBarTheme appBarTema() {
  return AppBarTheme(
      backgroundColor: SecondaryColor,
      elevation: 0,
      centerTitle: true,
      iconTheme: IconThemeData(color: PrimaryDarkColor));
}
