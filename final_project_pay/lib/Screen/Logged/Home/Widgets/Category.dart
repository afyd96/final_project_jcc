import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Category extends StatelessWidget {
  const Category({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            GestureDetector(
              onTap: () {},
              child: Column(
                children: [
                  SvgPicture.asset(
                    "assets/icons/coupon.svg",
                    height: 25,
                    width: 25,
                  ),
                  SizedBox(height: 5),
                  Text("Discount", textAlign: TextAlign.center)
                ],
              ),
            ),
            Column(
              children: [
                SvgPicture.asset(
                  "assets/icons/fashion woman.svg",
                  height: 25,
                  width: 25,
                ),
                SizedBox(height: 5),
                Text("Woman", textAlign: TextAlign.center)
              ],
            ),
            Column(
              children: [
                SvgPicture.asset(
                  "assets/icons/fashion man.svg",
                  height: 25,
                  width: 25,
                ),
                SizedBox(height: 5),
                Text("Man", textAlign: TextAlign.center)
              ],
            ),
            Column(
              children: [
                SvgPicture.asset(
                  "assets/icons/laptop.svg",
                  height: 25,
                  width: 25,
                ),
                SizedBox(height: 5),
                Text("Technology", textAlign: TextAlign.center)
              ],
            ),
            Column(
              children: [
                SvgPicture.asset(
                  "assets/icons/more.svg",
                  height: 25,
                  width: 25,
                ),
                SizedBox(height: 5),
                Text("More", textAlign: TextAlign.center)
              ],
            ),
          ]),
    );
  }
}
