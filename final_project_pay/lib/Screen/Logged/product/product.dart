import 'package:final_project_pay/Screen/Logged/Cart/CartScreen.dart';
import 'package:final_project_pay/Screen/Logged/product/producttile.dart';
import 'package:final_project_pay/shared/components/CustomNavBot.dart';
import 'package:final_project_pay/shared/components/customiconcounter.dart';
import 'package:final_project_pay/shared/components/productcontroller.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:get/instance_manager.dart';

class Product extends StatelessWidget {
  Product({Key? key}) : super(key: key);
  final ProductController productController = Get.put(ProductController());
  static String routeName = "Screen/Logged/product";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.product),
      backgroundColor: SecondaryColor,
      body: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //Bagian SearchField
                  Container(
                    width: MediaQuery.of(context).size.width * 0.6,
                    decoration: BoxDecoration(
                      color: SecondaryColor.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: TextField(
                      onChanged: (value) => print(value),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 15),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          hintText: "Search product",
                          prefixIcon: Icon(Icons.search)),
                    ),
                  ),
                  CustomIconCounter(
                      svgSrc: "assets/icons/shopping-cart.svg",
                      numOfitem: 0,
                      press: () =>
                          Navigator.pushNamed(context, CartScreen.routeName)),
                  CustomIconCounter(
                      svgSrc: "assets/icons/bell.svg",
                      numOfitem: 2,
                      press: () {}),
                ],
              ),
            ),
            Expanded(
              child: Obx(() {
                if (productController.isLoading.value)
                  return Center(child: CircularProgressIndicator());
                else
                  return StaggeredGridView.countBuilder(
                    crossAxisCount: 2,
                    itemCount: productController.productList.length,
                    crossAxisSpacing: 16,
                    mainAxisSpacing: 16,
                    itemBuilder: (context, index) {
                      return ProductTile(productController.productList[index]);
                    },
                    staggeredTileBuilder: (index) => StaggeredTile.fit(1),
                  );
              }),
            )
          ],
        ),
      ),
    );
  }
}
