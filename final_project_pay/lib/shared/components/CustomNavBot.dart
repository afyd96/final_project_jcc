import 'package:final_project_pay/Screen/Logged/Home/Home.dart';
import 'package:final_project_pay/Screen/Logged/product/product.dart';
import 'package:final_project_pay/Screen/Logged/profile/profile.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

enum MenuState { home, product, favorite, profile }

class CustomBottomNavBar extends StatelessWidget {
  const CustomBottomNavBar({
    Key? key,
    required this.selectedMenu,
  }) : super(key: key);

  final MenuState selectedMenu;

  @override
  Widget build(BuildContext context) {
    final Color inActiveIconColor = SecondaryColor;
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
        color: PrimaryDarkColor,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, -15),
            blurRadius: 20,
            color: Color(0xFFDADADA).withOpacity(0.15),
          ),
        ],
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(50),
          topRight: Radius.circular(50),
        ),
      ),
      child: SafeArea(
          top: false,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                icon: SvgPicture.asset(
                  "assets/icons/store.svg",
                  color: MenuState.home == selectedMenu
                      ? PrimaryLightColor
                      : inActiveIconColor,
                ),
                onPressed: () => Navigator.pushNamed(context, Home.routeName),
              ),
              IconButton(
                icon: SvgPicture.asset(
                  "assets/icons/new-product.svg",
                  color: MenuState.product == selectedMenu
                      ? PrimaryLightColor
                      : inActiveIconColor,
                ),
                onPressed: () {
                  Navigator.pushNamed(context, Product.routeName);
                },
              ),
              IconButton(
                icon: SvgPicture.asset(
                  "assets/icons/heart.svg",
                  color: MenuState.favorite == selectedMenu
                      ? PrimaryLightColor
                      : inActiveIconColor,
                ),
                onPressed: () {},
              ),
              IconButton(
                  icon: SvgPicture.asset(
                    "assets/icons/profile-user.svg",
                    color: MenuState.profile == selectedMenu
                        ? PrimaryLightColor
                        : inActiveIconColor,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, Profile.nameRoute);
                  }),
            ],
          )),
    );
  }
}
