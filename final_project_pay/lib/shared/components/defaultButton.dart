import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({Key? key, required this.text, required this.press})
      : super(key: key);

  final String? text;
  final Function? press;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
              side: BorderSide(color: PrimaryColor, width: 2)),
          primary: PrimaryDarkColor,
        ),
        onPressed: press as void Function()?,
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 10),
            alignment: Alignment.center,
            width: double.infinity,
            child: Text(
              text!,
              style: TextStyle(fontSize: 20, color: TextDarkColor),
            )));
  }
}
