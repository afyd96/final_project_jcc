import 'package:final_project_pay/Screen/non_logged/Register/widgets/registerform.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 10),
                Text("Register Account",
                    style: TextStyle(color: PrimaryColor, fontSize: 20)),
                SizedBox(height: 10),
                Text(
                  "Complete details below\n for create account",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
                registerform(),
                SizedBox(height: 10),
                Text(
                  'By continuing your register that you agree \nwith our Term and Condition',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
