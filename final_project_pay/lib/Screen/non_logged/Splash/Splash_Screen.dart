import 'package:final_project_pay/Screen/non_logged/Login/LoginScreen.dart';
import 'package:final_project_pay/Screen/non_logged/Station/station.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  static String routeName = "/Screen/non_logged/Splash";

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    var duration = Duration(seconds: 10);
    //delay 10 detik sebelum ke station login/register
    Future.delayed(duration, () {
      Navigator.pushNamedAndRemoveUntil(
          context, Station.routeName, (route) => false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TextDarkColor,
      body: Container(
        decoration: BoxDecoration(
            image:
                DecorationImage(image: AssetImage("assets/images/splash.gif"))),
      ),
    );
  }
}
