import 'package:final_project_pay/Screen/Logged/Home/Widgets/Section.dart';
import 'package:final_project_pay/models/product_model.dart';
import 'package:final_project_pay/shared/components/productCard.dart';
import 'package:flutter/material.dart';

class product extends StatelessWidget {
  const product({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Section(title: "Popular Products", press: () {}),
        ),
        SizedBox(height: 20),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Row(
            children: [
              ...List.generate(
                demoProducts.length,
                (index) {
                  if (demoProducts[index].isPopular)
                    return ProductCard(product: demoProducts[index]);

                  return SizedBox.shrink();
                },
              ),
              SizedBox(width: 20),
            ],
          ),
        )
      ],
    );
  }
}
