import 'package:flutter/material.dart';

class UserModel {
  String? uid;
  String? email;
  String? firstname;
  String? lastname;
  String? address;
  String? mobilephone;

  UserModel({
    this.uid,
    this.email,
    this.firstname,
    this.lastname,
    this.address,
    this.mobilephone,
  });

  // receive data from server

  factory UserModel.fromMap(map) {
    return UserModel(
      uid: map['uid'],
      email: map['email'],
      firstname: map['firstname'],
      lastname: map['lastname'],
      address: map['address'],
      mobilephone: map['mobilephone'],
    );
  }
// sending data to our server

  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'email': email,
      'firstname': firstname,
      'lastname': lastname,
      'address': address,
      'mobilephone': mobilephone
    };
  }
}
