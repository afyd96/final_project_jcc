import 'package:final_project_pay/Screen/Logged/profile/Widgets/profilebody.dart';
import 'package:final_project_pay/Screen/Logged/profile/Widgets/profilemenu.dart';
import 'package:final_project_pay/Screen/Logged/profile/Widgets/profilepict.dart';
import 'package:final_project_pay/shared/components/CustomNavBot.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);
  static String nameRoute = "Screen/Logged/profile";
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SecondaryColor,
      appBar: AppBar(
        title: Text("Profile Menu",
            style: TextStyle(
                color: PrimaryColor,
                fontSize: 32,
                fontWeight: FontWeight.w700)),
      ),
      body: profileBody(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.profile),
    );
  }
}
