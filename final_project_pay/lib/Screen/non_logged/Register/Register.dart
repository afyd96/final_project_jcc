import 'package:final_project_pay/Screen/non_logged/Register/widgets/body.dart';
import 'package:final_project_pay/shared/constant/colors.dart';
import 'package:final_project_pay/shared/constant/theme.dart';
import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);
  static String routeName = "Screen/non_logged/Register";

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: SecondaryColor,
      appBar: AppBar(
        title: Text("Register", style: TextStyle(color: PrimaryDarkColor)),
      ),
      body: Body(),
    );
  }
}
