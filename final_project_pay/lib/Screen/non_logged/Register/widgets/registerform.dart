import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:final_project_pay/Screen/Logged/Home/Home.dart';
import 'package:final_project_pay/models/user_model.dart';
import 'package:final_project_pay/shared/components/customSuffixIcon.dart';
import 'package:final_project_pay/shared/components/defaultButton.dart';
import 'package:final_project_pay/shared/components/formerror.dart';
import 'package:final_project_pay/shared/constant/validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class registerform extends StatefulWidget {
  const registerform({Key? key}) : super(key: key);

  @override
  _registerformState createState() => _registerformState();
}

class _registerformState extends State<registerform> {
  //controller
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _conformPasswordController =
      TextEditingController();

  // firebase
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  final _formKey = GlobalKey<FormState>();

  bool remember = false;
  final List<String?> errors = [];

  void addError({String? error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String? error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          // emailfield
          TextFormField(
            controller: _emailController,
            keyboardType: TextInputType.emailAddress,
            onSaved: (newValue) => _emailController.text = newValue!,
            onChanged: (value) {
              if (value.isNotEmpty) {
                removeError(error: kEmailNullError);
              } else if (emailValidatorRegExp.hasMatch(value)) {
                removeError(error: kInvalidEmailError);
              }
              return null;
            },
            validator: (value) {
              if (value!.isEmpty) {
                addError(error: kEmailNullError);
                return "";
              } else if (!emailValidatorRegExp.hasMatch(value)) {
                addError(error: kInvalidEmailError);
                return "";
              }
              return null;
            },
            decoration: InputDecoration(
              labelText: "Email",
              hintText: "Enter your email",
              suffixIcon: CustomSuffixIcon(svgicon: "assets/icons/email.svg"),
            ),
          ),
          SizedBox(height: 30),
          //password field
          TextFormField(
            controller: _passwordController,
            obscureText: true,
            onSaved: (value) => _passwordController.text = value!,
            onChanged: (value) {
              setState(() {
                if (value.isNotEmpty) {
                  removeError(error: kPassNullError);
                } else if (value.length >= 8) {
                  removeError(error: kShortPassError);
                }
                return null;
              });
            },
            validator: (value) {
              setState(() {
                if (value!.isEmpty) {
                  addError(error: kPassNullError);
                } else if (value.length < 8) {
                  addError(error: kShortPassError);
                }
                return null;
              });
            },
            decoration: InputDecoration(
              labelText: "Password",
              hintText: "Enter your password",
              suffixIcon: CustomSuffixIcon(svgicon: "assets/icons/padlock.svg"),
            ),
          ),
          SizedBox(height: 30),
          TextFormField(
            controller: _conformPasswordController,
            obscureText: true,
            onChanged: (value) {
              setState(() {
                if (value.isNotEmpty) {
                  removeError(error: kPassNullError);
                } else if (_passwordController.text !=
                    _conformPasswordController.text) {
                  removeError(error: kMatchPassError);
                }
                return null;
              });
            },
            validator: (value) {
              setState(() {
                if (value!.isEmpty) {
                  addError(error: kPassNullError);
                } else if (_passwordController.text !=
                    _conformPasswordController.text) {
                  addError(error: kMatchPassError);
                }
                return null;
              });
            },
            decoration: InputDecoration(
              labelText: "Confirm Password",
              hintText: "Re-enter your password",
              suffixIcon: CustomSuffixIcon(svgicon: "assets/icons/padlock.svg"),
            ),
          ),
          FormError(errors: errors),
          SizedBox(height: 40),
          DefaultButton(
            text: "Register",
            press: () async {
              if (_formKey.currentState!.validate()) {
                _formKey.currentState!.save();
                // Kalo valid pergi ke halaman home
                await _firebaseAuth
                    .createUserWithEmailAndPassword(
                        email: _emailController.text,
                        password: _passwordController.text)
                    .then((value) => {postDetailsToFireStore()});
              }
            },
          ),
        ],
      ),
    );
  }

  postDetailsToFireStore() async {
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = _firebaseAuth.currentUser;

    UserModel userModel = UserModel();

    userModel.email = user!.email;
    userModel.uid = user.uid;

    await firebaseFirestore
        .collection("user")
        .doc(user.uid)
        .set(userModel.toMap());
    Fluttertoast.showToast(msg: "Account Created Success");
    Navigator.pushNamedAndRemoveUntil(
        context, Home.routeName, (route) => false);
  }
}
